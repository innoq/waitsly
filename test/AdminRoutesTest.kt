package io.innoq

import io.ktor.http.*
import kotlin.test.*
import io.ktor.server.testing.*

class AdminRoutesTest {
    @Test
    fun testNewForm() {
        withTestApplication({ module() }) {
            handleRequest(HttpMethod.Get, "/admin/new").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                assertNotNull(response.content)
                assertTrue(response.content!!.contains("Neue Warteliste"))
            }
        }
    }
}
