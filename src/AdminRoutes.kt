package io.innoq

import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.ByteArrayContent
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.thymeleaf.ThymeleafContent
import io.ktor.util.getOrFail
import net.glxn.qrgen.core.image.ImageType
import net.glxn.qrgen.javase.QRCode

fun Route.adminRoutes() {
    route("/admin") {
        get("/new") {
            call.respond(ThymeleafContent("admin/new", emptyMap()))
        }

        post("/new") {
            val name = call.receiveParameters()["name"]
            if (name != null && !name.isBlank()) {
                val waitingList = createNewWaitingList(name)
                call.respondRedirect("/admin/${waitingList.slug}/${waitingList.adminToken}")
            } else {
                call.respond(ThymeleafContent("admin/new", mapOf("error" to "Bitte Namen angeben")))
            }
        }

        get("/{slug}/{adminToken}") {
            val slug = call.parameters.getOrFail("slug")
            val adminToken = call.parameters.getOrFail("adminToken")

            val waitingList = findWaitingListBySlugAndAdminToken(slug, adminToken)

            if (waitingList == null) {
                call.respond(HttpStatusCode.NotFound)
            } else {
                call.respond(ThymeleafContent("admin/details", mapOf("waitingList" to waitingList)))
            }
        }

        get("/{slug}/{adminToken}/current") {
            val slug = call.parameters.getOrFail("slug")
            val adminToken = call.parameters.getOrFail("adminToken")

            val waitingList = findWaitingListBySlugAndAdminToken(slug, adminToken)

            if (waitingList == null) {
                call.respond(HttpStatusCode.NotFound)
            } else {
                call.respond(ThymeleafContent("admin/current", mapOf("waitingList" to waitingList)))
            }
        }

        post("/{slug}/{adminToken}/next") {
            val slug = call.parameters.getOrFail("slug")
            val adminToken = call.parameters.getOrFail("adminToken")

            val waitingList = findWaitingListBySlugAndAdminToken(slug, adminToken)

            if (waitingList == null) {
                call.respond(HttpStatusCode.NotFound)
            } else {
                call.respondRedirect("/admin/${slug}/${adminToken}/current")
            }
        }

        get("/{slug}/{adminToken}/qrcode") {
            val slug = call.parameters.getOrFail("slug")
            val adminToken = call.parameters.getOrFail("adminToken")

            val waitingList = findWaitingListBySlugAndAdminToken(slug, adminToken)

            if (waitingList == null) {
                call.respond(HttpStatusCode.NotFound)
            } else {
                call.respond(ByteArrayContent(QRCode.from("https://waitsly.herokuapp.com/${waitingList.slug}").withSize(300, 300).to(ImageType.JPG).stream().toByteArray(), ContentType.Image.JPEG))
            }
        }

        get("/{slug}/{adminToken}/print") {
            val slug = call.parameters.getOrFail("slug")
            val adminToken = call.parameters.getOrFail("adminToken")

            val waitingList = findWaitingListBySlugAndAdminToken(slug, adminToken)

            if (waitingList == null) {
                call.respond(HttpStatusCode.NotFound)
            } else {
                call.respond(ThymeleafContent("admin/print", mapOf("waitingList" to waitingList)))
            }
        }
    }
}
