package io.innoq

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.ByteArrayContent
import io.ktor.http.content.OutputStreamContent
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.locations.Location
import io.ktor.locations.Locations
import io.ktor.locations.get
import io.ktor.request.path
import io.ktor.request.receiveParameters
import io.ktor.response.*
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.thymeleaf.Thymeleaf
import io.ktor.thymeleaf.ThymeleafContent
import io.ktor.util.getOrFail
import net.glxn.qrgen.core.image.ImageType
import org.slf4j.event.Level
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver
import net.glxn.qrgen.javase.QRCode

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(Thymeleaf) {
        setTemplateResolver(ClassLoaderTemplateResolver().apply {
            prefix = "templates/thymeleaf/"
            suffix = ".html"
            characterEncoding = "utf-8"
        })
    }

    install(Locations) {
    }

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    routing {
        get("/") {
            call.respond(ThymeleafContent("index", emptyMap()))
        }

        get("/qrcode/{content}") {
            val content = call.parameters["content"]
            call.respond(ByteArrayContent(QRCode.from(content).to(ImageType.JPG).stream().toByteArray(), ContentType.Image.JPEG))
        }

        get("/{slug}") {
            val slug = call.parameters.getOrFail("slug")
            val waitingList = findWaitingListBySlug(slug)

            if (waitingList == null) {
                call.respond(HttpStatusCode.NotFound)
            } else {
                call.respond(ThymeleafContent("show", mapOf("waitingList" to waitingList)))
            }
        }

        post("/{slug}") {
            val slug = call.parameters.getOrFail("slug")
            val waitingList = findWaitingListBySlug(slug)

            if (waitingList == null) {
                call.respond(HttpStatusCode.NotFound)
            } else {
                val slot = createNextSlot(waitingList)
                call.respondRedirect("/${slug}/slot/${slot.number}", false)
            }
        }

        get("/{slug}/slot/{number}") {
            val slug = call.parameters.getOrFail("slug")
            val number = call.parameters.getOrFail("number").toInt()
            val waitingList = findWaitingListBySlug(slug)

            if (waitingList == null) {
                call.respond(HttpStatusCode.NotFound)
            } else {
                val slot = waitingList.findSlot(number)
                if (slot == null){
                    call.respond(HttpStatusCode.NotFound)
                } else {
                    call.respond(ThymeleafContent("slot", mapOf("waitingList" to waitingList, "slot" to slot)))
                }
            }
        }


        adminRoutes();

        static("/static") {
            resources("static")
        }
    }
}
