package io.innoq

data class WaitingList(val name: String,
                       val slug: String,
                       val adminToken: String,
                       val waitingNumbers: List<WaitingSlot>) {

    fun currentSlot(): WaitingSlot?{
        return WaitingSlot(17, false)
    }

    fun nextSlot(): WaitingSlot {
        val nextNumber = waitingNumbers.lastOrNull()?.number ?: 0
        return WaitingSlot(nextNumber + 1, false)
    }

    fun findSlot(number: Number): WaitingSlot?{
        return waitingNumbers.filter { it.number == number }.singleOrNull()
    }
}

data class WaitingSlot(val number: Int, val processed: Boolean)


val waitingLists = mutableListOf<WaitingList>()

fun createNewWaitingList(name: String): WaitingList {
    val waitingList = WaitingList(name, "slug-${name}", "admin-token-${name}", emptyList())
    waitingLists.add(waitingList)
    return waitingList
}

fun findWaitingListBySlug(slug: String): WaitingList? {
    return waitingLists.filter { it.slug.equals(slug) }.singleOrNull()
}

fun findWaitingListBySlugAndAdminToken(slug: String, adminToken: String): WaitingList? {
    return waitingLists.filter { it.slug == slug && it.adminToken == adminToken }.singleOrNull()
}

fun createNextSlot(waitingList: WaitingList): WaitingSlot {
    val newWaitingSlot = waitingList.nextSlot()

    val updatedWaitingList = waitingList.copy(waitingNumbers = waitingList.waitingNumbers + newWaitingSlot)

    waitingLists.removeIf {
        it.slug == waitingList.slug
    }
    waitingLists.add(updatedWaitingList)

    return newWaitingSlot
}
